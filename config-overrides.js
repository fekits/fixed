const path = require('path');
const paths = require('react-scripts/config/paths');
// 图片无损压缩插件
const TinypngWebpackPlugin = require('tinypng-webpack-plugin');
// 非侵入式修改打包配置插件
const { override, addBabelPlugin, addWebpackAlias, addPostcssPlugins, overrideDevServer } = require('customize-cra');

// 图片压缩KEY
const tinypngKey = [
  'nEkBOCe0lyFyd7v3bF6k2MjwLK4UvVX6',
  'WBr3Vm6ZqmrsRZaBBm9IXcluqF4CHY3i',
  'GvVhRewCjKFNUxpZ34AhSvFJwHHqKfah',
  'tm3CLBwcIJ9YJP8F3UE1RJWyD3GxOW2b',
  '8825ugKrWMSqjwNnos7p94xdJTUWP6hq'
];

// 打包环境和接口环境
let [env, api = ''] = process.env.npm_lifecycle_event.split(':');

// 开发环境打包
const start = env === 'start';
// 生产环境打包
const babel = env === 'babel';

// 修改输出文件夹为相对路径
paths.publicUrlOrPath = start ? '/' : './';

module.exports = {
  webpack: override(
    // 配置路径别名
    addWebpackAlias({
      '@': path.join(__dirname, 'src'),
      components: path.join(__dirname, 'src/components'),
      css: path.join(__dirname, 'src/assets/css'),
      img: path.join(__dirname, 'src/assets/img')
    }),
    addBabelPlugin(['@babel/plugin-proposal-decorators', { legacy: true }]),
    addPostcssPlugins([
      // PX转REM
      // require('postcss-pxtorem')({
      //   // 换算率
      //   rootValue: 100,
      //   propList: ['*'],
      //   // 设置最小转换值小于2px不转换
      //   minPixelValue: 2,
      //   // 过滤掉node_modules 文件夹下面的样式
      //   exclude: /node_modules/i
      // })
    ]),

    (config) => {
      // ES6语法加强编译
      config.entry = [
        require.resolve('core-js/stable'),
        require.resolve('regenerator-runtime/runtime'),
        // MOCK数据
        ...(api === 'mock' ? [require.resolve('./mock')] : []),
        // 移动端日志查看
        ...(env === 'build' ? [require.resolve('./vconsole')] : []),
        config.entry
      ];

      // 开发环境配置
      if (start) {
        // 可追踪源文件
        config.devtool = 'eval-source-map';
      } else {
        // 生产环境配置

        // 生产环境不要开发调试工具
        config.devtool = false;

        // 修改输出文件夹
        // paths.appBuild = path.join(path.dirname(paths.appBuild), 'dist');
        // config.output.path = path.join(path.dirname(config.output.path), 'dist');

        // 设置输出的JS文件只生成一个文件
        config.optimization.splitChunks = {};
        config.optimization.runtimeChunk = false;

        // 修改CSS文件名
        // console.log(config.plugins[5].options);
        // config.plugins[5].options.filename = 'css/[name].[contenthash:5].min.css';
        // config.plugins[5].options.chunkFilename = 'css/[name].[contenthash:5].chunk.css';
        // config.plugins[5].options = {
        //   filename: 'css/[name].[contenthash:5].min.css',
        //   chunkFilename: 'css/[name].[contenthash:5].chunk.css'
        // };

        // CSS压缩不输出MAP文件
        // config.optimization.minimizer[1].options.cssProcessorOptions.map = false;

        // 图片输出路经
        // console.log(JSON.stringify(config.module.rules[1]));
        // config.module.rules[1].oneOf[0].options.name = 'img/[name].[hash:5].[ext]';
        // config.module.rules[1].oneOf[7].options.name = 'plugin/[name].[hash:5].[ext]';

        // scss中的图片路径修改
        // config.module.rules[1].oneOf[5].use[0].options.publicPath = '../';

        // 删除GenerateSW
        // console.log(config.plugins);
        // config.plugins.splice(8, 1);

        // 图片加哈希值
        // config.module.rules[1].use[0].options.fallback.options.name = 'img/[name].[hash:5].[ext]';

        // 图片转成BASE6的阀值
        // config.module.rules[1].use[0].options.limit = 50000;

        // 设置不要生成 xxx.js.LICENSE.txt
        config['optimization']['minimizer'][0].options.extractComments = false;

        // 打包到线上环境使用，压缩图片和清理调试信息
        if (babel) {
          config.plugins.push.apply(config.plugins, [
            // 无损压缩图片
            new TinypngWebpackPlugin({
              key: tinypngKey
            })
          ]);
        }
      }

      return config;
    }
  ),
  devServer: overrideDevServer((config) => {
    return {
      ...config,
      proxy: {
        '/api': {
          target: 'https://cms.asnowsoft.com/api/',
          changeOrigin: true,
          secure: false,
          pathRewrite: {
            '^/api': ''
          }
        }
      }
    };
  })
};
