import { isElement, isString } from '@fekit/utils';

(function() {
  var lastTime = 0;
  var vendors = ['webkit', 'moz'];
  const win: any = window;
  for (var x = 0; x < vendors.length && !win.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = win[vendors[x] + 'RequestAnimationFrame'];
    window.cancelAnimationFrame = win[vendors[x] + 'CancelAnimationFrame'] || win[vendors[x] + 'CancelRequestAnimationFrame'];
  }

  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = function(callback) {
      var currTime = new Date().getTime();
      var timeToCall = Math.max(0, 16.7 - (currTime - lastTime));
      var id = window.setTimeout(function() {
        callback(currTime + timeToCall);
      }, timeToCall);
      lastTime = currTime + timeToCall;
      return id;
    };
  }
  if (!window.cancelAnimationFrame) {
    window.cancelAnimationFrame = function(id) {
      clearTimeout(id);
    };
  }
})();

const getDom = function(param: any) {
  return param ? (isElement(param) ? param : isString(param) ? document.querySelector(param) : null) : null;
};

const _class = (el: any, className: any, type: any) => {
  const reg = new RegExp(' *' + className + ' *', 'g');
  if (type === '+') {
    if (el.className.replace(/\s*/g, '')) {
      if (!reg.test(el.className)) {
        el.className += ' ' + className;
      }
    } else {
      el.className = className;
    }
  }
  if (type === '-') {
    el.className = el.className.replace(reg, ' ').replace(/(^\s*|\s+$)/g, '');
  }
};

const _rect = function(ele: any) {
  // 判断滚动方向和是否到顶部或底部
  let inHeight = window.innerHeight;
  let rect = ele.getBoundingClientRect();
  rect.isEnd = rect.bottom - inHeight <= 0;
  return rect;
};

class Fixed {
  el: any;
  pos: any;
  offset: any;
  _top: any;
  zIndex: any;
  area: any;
  wrap: any;
  fixedClass: any;
  fixedStyle: any;
  autoStyle: any;
  keepArea: any;
  on: any;
  status: any;
  working: any;
  top: any;
  wrapH: any;
  winH: any;
  constructor(param: any) {
    if (param) {
      // 要固定的元素
      this.el = getDom(param.el);
      // 位置
      this.pos = param.pos || 'top';
      // 元素固定位置
      this.offset = param.offset || 0;
      this._top = this.offset;
      // 元素层级
      this.zIndex = param.zIndex;
      // 元素监听区域
      this.area = param.area ? getDom(param.area) : null;
      // 动态参考元素
      this.wrap = param.wrap || this.el.parentElement;

      // 元素固定时的CLASS
      this.fixedClass = param.fixedClass || '';
      this.fixedStyle = param.fixedStyle || '';
      this.autoStyle = param.autoStyle;

      // 保留区域
      this.keepArea = param.hasOwnProperty('keepArea') ? param.keepArea : true;

      // 回调
      this.on = Object.assign({ fixed: () => {}, reduced: () => {} }, param.on);

      // 状态
      this.status = 'init';
      this.working = 0;
      this.restyle();

      // 监听滚动事件
      window.addEventListener('scroll', () => {
        this.tofixed();
      });
      // 监听窗口大小
      window.addEventListener('resize', () => {
        this.restyle();
        this.tofixed();
      });

      // 立即监听一次
      this.tofixed();
    }
  }

  restyle(): any {
    this.winH = window.innerHeight;
    // 动态参考元素设置成与要固定的元素一样的尺寸
    this.wrapH = this.el.offsetHeight;
    if (this.keepArea) {
      this.wrap.style.cssText = `height:${this.wrapH}px`;
    }
    if (this.autoStyle) {
      this.fixedStyle = this.fixedClass
        ? `${this.pos}:${this.offset}px;`
        : this.fixedStyle || `position:fixed; width:${this.el.offsetWidth}px; ${this.pos}:${this.offset}px; ${this.zIndex ? 'z-index:' + this.zIndex : ''}`;
    }
  }

  tofixed(): any {
    const _fix: any = () => {
      // 进入工作状态
      this.working = 0;
      // 获取元素属性
      let wrapRect = _rect(this.wrap);
      let areaRect = this.area ? _rect(this.area) : { bottom: 1 };
      // 判断是否悬浮
      const isFixed = this.pos === 'top' ? wrapRect.top < this.offset + 1 && areaRect.bottom > 0 : wrapRect.top > this.winH - this.offset - wrapRect.height;
      // 顶部悬浮
      if (isFixed) {
        // 设置悬浮属性
        if (this.autoStyle) {
          this.el.style.cssText = this.fixedStyle;
        }
        if (this.status !== 'fixed') {
          this.fixedClass && _class(this.el, this.fixedClass, '+');
          this.status = 'fixed';
          this.wrap.setAttribute('data-status', this.status);
          this.on.fixed(this);
        }
        // 设置区域的尾部跟随
        if (this.pos === 'top' && this.area) {
          let top = areaRect.bottom < this.offset + this.wrapH ? this.offset + areaRect.bottom - (this.offset + this.wrapH) : this.offset;
          if (top !== this.top) {
            this.el.style[this.pos] = top + 'px';
            this.top = top;
          }
        }
      } else {
        // 还原
        if (this.autoStyle) {
          this.el.style.cssText = '';
        }
        if (this.status !== 'reduced') {
          this.status = 'reduced';
          this.fixedClass && _class(this.el, this.fixedClass, '-');
          this.wrap.setAttribute('data-status', this.status);
          this.on.reduced(this);
        }
      }
      this.working = 0;
    };

    if (this.working) {
      return false;
    } else {
      cancelAnimationFrame(_fix);
      requestAnimationFrame(_fix);
    }
  }

  refresh() {
    this.tofixed();
  }
}

export default Fixed;
