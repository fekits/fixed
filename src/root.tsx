import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import Fixed from './npm/src';

const store = ({ _sys = {} }: any) => ({ _sys });
const event = (dispatch: any) => {
  return {};
};
function Root(props: any) {
  useEffect(() => {
    new Fixed({
      el: '#f1_tab',
      zIndex: 1001,
      keepArea: true,
      fixedClass: 'f1_fixed',
      on: {
        fixed() {
          console.log('1固定了');
        },
        reduced() {
          console.log('1还原了');
        }
      }
    });

    new Fixed({
      el: document.getElementById('f3_tab'),
      area: '#f3',
      keepArea: true,
      offset: 60,
      autoStyle: true,
      on: {
        fixed() {
          console.log('2固定了');
        },
        reduced() {
          console.log('2还原了');
        }
      }
    });

    new Fixed({
      el: document.getElementById('f7_tab'),
      area: '#f7',
      keepArea: true,
      pos: 'bottom',
      autoStyle: true,
      on: {
        fixed() {
          console.log('2固定了');
        },
        reduced() {
          console.log('2还原了');
        }
      }
    });
  }, []);

  return (
    <div>
      <h1>MC-FIXED</h1>
      <br />
      <p className="ac co-red">这个页面的示例代码如下:</p>
      <pre lang="javascript">
        {`
    // 代码着色自豪地采用mc-tinting

    // 引入插件
    import McFixed from 'mc-fixed';
    
    // 设置一个不设定作用范围的实例,默认为整个body区域，顶部为0时固定的实例，并且设置层级为1001,固定时触发一个回调。
    new McFixed({
      el: '#f1_tab',  // 可以直接写选择器，也可以写获取到的DOM元素如document.getElementById('f1_tab')
      zIndex: 1001,
      fixedClass: 'f1_fixed',  // 固定时的样式可以写在这个class里
      on: {
        fixed: ()=>{
          console.log('固定了');
        },
        reduced: ()=>{
          console.log('还原了');
        }
      }
    });
    
    // 新建一个在ID为f3的区域范围内滚动到顶部60px时固定的实例。
    new McFixed({
      el: document.getElementById('f3_tab'), // 这里也可以直接写'#f3_tab'
      area: document.getElementById('f3'),   // 这里也可以直接写 '#f3'
      top: 60
    });
    `}
      </pre>
      <br />
      <p className="ac co-red">请滚动页面查看效果</p>
      <div className="floors">
        <div id="f1" className="style-floor" data-track="120000001" style={{ height: '300px' }}>
          <div>
            <div id="f1_tab" className="mc-floor-nav z-ll">
              这个在整个文档内固定
            </div>
          </div>
        </div>
        <div id="f2" className="style-floor" data-track="120000002">
          2F
        </div>
        <div id="f3-box" style={{ paddingBottom: '50%', position: 'relative', height: '1100px', border: '1px dotted #000', textAlign: 'center' }}>
          <p style={{ color: ' #999' }}>offsetTop多层定位测试</p>
          <div style={{ position: 'absolute', top: '200px', left: 0, width: '100%', border: '1px dotted #ff0000', padding: '20px' }}>
            <div id="f3" className="style-floor">
              <div>
                <div id="f3_tab" className="z-lm">
                  这个只在F3区域内固定
                </div>
              </div>
              <p>3F</p>
            </div>
          </div>
        </div>
        <div id="f4" className="style-floor">
          4F
        </div>
        <div id="f5" className="style-floor">
          5F
        </div>
        <div id="f6" className="style-floor">
          F6
        </div>
        <div id="f7" className="style-floor">
          <div className="f7-wrap">
            <div id="f7_tab" className="z-ls">
              底部悬浮
            </div>
          </div>
        </div>
        <div id="f8" className="style-floor">
          8F
        </div>
      </div>
    </div>
  );
}

export default connect(store, event)(React.memo(Root));
